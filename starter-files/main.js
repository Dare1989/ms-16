// Your code starts here

// EXERCISE 01
class Que {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  isAdult() {
    if (this.age >= 18) {
      return true;
    } else {
      return false;
    }
  }
}

let queList = [];

class Guest extends Que {
  constructor(name, age) {
    super(name, age);
  }
}

let guestList = [];

// EXERCISE 02
let btn = document.querySelector("#addToQue");
let queTable = document.querySelector("#queList");
let guestTable = document.querySelector("#guestList");

btn.addEventListener("click", function () {
  let que1 = new Que("Name", 35);
  queList.push(que1);

  let tr = document.createElement("tr");
  let td1 = document.createElement("td");
  let td2 = document.createElement("td");
  let td3 = document.createElement("td");
  let btnAccept = document.createElement("button");
  let btnDecline = document.createElement("button");

  btnAccept.classList.add("btn", "btn-sm", "btn-outline-success");
  btnDecline.classList.add("btn", "btn-sm", "btn-outline-danger", "mr-1");

  tr.appendChild(td1);
  tr.appendChild(td2);
  tr.appendChild(td3);
  td3.appendChild(btnAccept);
  td3.appendChild(btnDecline);

  td1.innerText = que1.name;
  td2.innerText = que1.age;
  btnAccept.innerText = "Accept";
  btnDecline.innerText = "Decline";

  queTable.appendChild(tr);

  let editMode = false;

  // EXERCISE 03
  btnAccept.addEventListener("click", function (e) {
    if (guestList.length > 15) {
      alert("The limit is reached");
    } else {
      let editBtn = document.createElement("button");
      editBtn.classList.add("btn", "btn-outline-info", "btn-sm");
      editBtn.innerText = "Edit";

      guestList.push(que1);

      let trGuest = document.createElement("tr");
      let tdName = document.createElement("td");
      let tdAge = document.createElement("td");
      let tdEdit = document.createElement("td");

      trGuest.appendChild(tdName);
      trGuest.appendChild(tdAge);
      trGuest.appendChild(tdEdit);

      tdName.innerText = que1.name;
      tdAge.innerText = que1.age;
      tdEdit.appendChild(editBtn);

      guestTable.appendChild(trGuest);

      let row = e.target.parentElement.parentElement;
      row.remove();

      //   EXERCISE04
      editBtn.addEventListener("click", function () {
        editMode = true;
        let input = document.createElement("input");
        input.classList.add("form-control", "form-control-sm");
        tdName.innerHTML = "";
        tdName.appendChild(input);
        editBtn.innerText = "Save";
        // EXERCISE05
        editBtn.addEventListener("click", function () {
          let inputVal = input.value;
          guestList.push(inputVal);
          tdName.innerHTML = inputVal;
          editBtn.innerText = "Edit";
        });
      });
    }
  });

  btnDecline.addEventListener("click", function (e) {
    e.target.parentElement.parentElement.remove();
  });
  console.log("QUE:" + queList);
  console.log("GUEST" + guestList);
});
